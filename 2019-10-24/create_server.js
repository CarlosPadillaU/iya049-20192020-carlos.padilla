const events = require("events");
const net = require("net");

//check if the las  charts of the buffer are \r or \n
const isFinish = buffer => {
  const start = buffer.length - 6;
  const end = buffer.length;

  let flag = true;

  //get las 6 charts of the buffer
  const charts = buffer.substring(start, end).split("");

  for (const c of charts) {
    if (!(c == "\r" || c == "\n")) {
      flag = false;
      break;
    }
  }

  return flag;
};

const createServer = () => {
  // ... ✏️

  class Emitter extends events {}
  const eventEmitter = new Emitter();

  let buffer = "";
  const server = net.createServer(socket => {
    socket.on("end", () => {
      buffer = "";
    });

    socket.on("data", data => {
      buffer += Buffer.from(data).toString("utf-8");

      if (!isFinish(buffer)) return;

      let aux = buffer.substring(0, buffer.length - 4);

      //infoArray es el array que contiene por posicion el startline [0], los header[1] y el body al final[2].
      let infoArray = aux.split("\r\n");

      //Aquí se usa este array para separar la información obtenida en el string
      const resources = infoArray[0].split(" ");

      infoArray.shift();

      const request = {
        method: resources[0],
        path: resources[1],
        protocol: resources[2],
        headers: infoArray.filter(info => info != "")
      };

      eventEmitter.emit("request", request, {
        send: (info, headers, body) => {
          socket.write(`HTTP/1.1 ${info}\r\n`);

          Object.entries(headers).forEach(([key, value]) => {
            socket.write(`${key}: ${value}\r\n`);
          });

          socket.end(`\r\n${body}\r\n`);
        }
      });
    });
  });

  return {
    on: (event, cb) => {
      eventEmitter.on(event, cb);
    },
    listen: port => {
      server.listen(port);
    }
  };
};

module.exports = createServer;
