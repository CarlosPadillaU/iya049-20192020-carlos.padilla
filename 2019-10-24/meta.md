## Integrantes del grupo.

... ✏️

- Carlos Padilla, Sergio Noh Puch
- Ricardo Amendolia, Sergio Dominguez

## Desglose de tareas

- Implementar la función `Creacion de on, listen y creacion de evennt emiiter y creacion de servidor`.

Asignado a Dominguez, Sergio

- Implementa funcion `isFinish`.

Asignado a Noh Puch, Sergio

- Implementa funcion `parseo de informacion`.

Asignado a Amendolia, Ricardo

- Implementa funcion `funcion send, event emiter request, limpieza de buffer`.

Asignado a Padilla, Carlos
