class Enemy {
  constructor() {
    this.parent = null;
    this.enemy = null;
    this.pos = 0;
  }
  render() {
    const enemyIcon = document.createElement("img");
    enemyIcon.src = sprites.invader1;
    this.enemy = document.createElement("div");
    this.enemy.setAttribute("class", "enemy");
    this.enemy.style.marginLeft = `${this.pos}px`;
    this.enemy.appendChild(enemyIcon);
  }
}
