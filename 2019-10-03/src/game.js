class Game {
  constructor() {
    this.enemies = [[...new Array(5)], [...new Array(5)], [...new Array(5)]];
    this.lifes = [...new Array(3)];
    this.enemiesMoveInterval = null;
    this.enemiesAttackInterval = null;
    this.stopAttack = false;
    this.lifesCount = 3;
  }

  rowEnemies(enemies) {
    const div = document.createElement("div");
    div.setAttribute("class", "enemy-row");

    const enemiesInstance = enemies.map(enemy => {
      const en = new Enemy();
      en.render();
      div.append(en.enemy);
      return en;
    });

    document.getElementById("js-move-enemies").appendChild(div);

    return enemiesInstance;
  }

  rowLifes(lifes) {
    const divL = document.createElement("div");
    divL.setAttribute("class", "life-row");

    const lifesInstance = lifes.map(li => {
      const gameinfo = new GameInfo();
      gameinfo.render();
      divL.append(gameinfo.herolifes);
      return gameinfo;
    });

    document.getElementById("gameinfo").appendChild(divL);

    return lifesInstance;
  }

  moveEnemies() {
    let pos = 0;
    let down = 0;
    let movePx = 10;
    let direction = "left"; //left right
    const div = document.getElementById("js-move-enemies");
    //300

    const reachLimit = () => {
      if (pos == 300) {
        direction = "right";
        down += 10;
      }
      if (pos == -290) {
        direction = "left";
        down += 10;
      }
    };
    const moveTo = () => {
      const move = {
        left: () => {
          pos += movePx;
        },
        right: () => {
          pos -= movePx;
        }
      };
      move[direction]();
    };

    //   return;
    this.enemiesMoveInterval = setInterval(() => {
      reachLimit();
      moveTo();
      div.style.marginLeft = `${pos}px`;
      div.style.marginTop = `${down}px`;
    }, 50);
  }

  respawnHero() {
    const hero = document.getElementsByClassName("hero")[0];
    hero.classList.remove("die");
  }

  heroDamage() {
    this.stopAttack = true;

    setTimeout(() => {
      this.respawnHero();
      this.stopAttack = false;
      this.lifesCount--;
      const vidas = document.getElementsByClassName("lifes")[this.lifesCount];
      vidas.classList.add("die");
      //const newLifes = this.rowLifes([...new Array(this.lifesCount)]);
    }, 2000);

    if (!this.lifesCount) this.GameOver();
    //implementar vidas
    //if(vidas == 0) this.GameOver();
  }

  GameOver() {
    alert("You Die");

    clearInterval(this.enemiesMoveInterval);
    clearInterval(this.enemiesAttackInterval);
    const bullets = document.getElementsByClassName("bullet");

    for (const bullet of bullets) {
      bullet.remove();
    }
    startGame();
  }

  enemiesAttack() {
    //select 5 randmon enemies to shoot
    const enemies = Array.prototype.slice.call(
      document.querySelectorAll(".enemy:not(.die)")
    );
    const selectedEnemies = enemies.sort(() => 0.5 - Math.random()).slice(0, 5);

    selectedEnemies.forEach(enemy => {
      const { x, y } = enemy.getBoundingClientRect();
      new Bullet({
        target: "hero",
        direction: "down",
        coords: { x, y }
      }).shoot(this.heroDamage.bind(this));
    });
  }

  start() {
    const hero = new Hero();
    hero.render(document.getElementById("hero-container"));
    const newEnemies = this.enemies.map(en => this.rowEnemies(en));
    const newLifes = this.rowLifes(this.lifes);
    //this.rowLifes(this.lifes);

    this.moveEnemies();

    this.enemiesAttackInterval = setInterval(() => {
      if (!this.stopAttack) this.enemiesAttack();
    }, 2000);
  }
}

const startGame = () => {
  const start = document.createElement("a");
  start.id = "start-game";
  start.innerHTML = "Click para comenzar";

  document.getElementById("warZone").innerHTML = `
  <div id="enemies-container">
    <div id="js-move-enemies"></div>
  </div>
  <div id="hero-container"></div>`;

  document.getElementById("warZone").prepend(start);

  start.addEventListener("click", () => {
    start.remove();
    new Game().start();
  });
};

startGame();
