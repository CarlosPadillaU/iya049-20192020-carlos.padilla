# Ejercicio 10-03

El ejercicio consiste en implementar una página web que contenga un juego de memoria.

## Fecha de entrega

El _pull request_ para la entrega del ejercicio debe crearse antes de `2019-10-17T12:00:00+02:00`.

## Enunciado

Modifica el archivo `index.html` para implementar el space invaders.

## Tecnologías

El ejercicio se debe realizar sin utilizar librerías externas.

El layout de la página debe estár implementado utilizando Flexbox.

## Entregables

Todos los archivos de esta carpeta son entregrables. La carpeta debe incluir, al menos,

* Un archivo `index.html` con la solución del ejercicio.
* Un archivo `meta.md` con el desglose de tareas por alumno, etc.

Los alumnos del grupo colaborarán en la rama que estará creada con ese propósito.

Cada alumno debe hacer al menos un commit por cada tarea que tenga asignada.

El método de entrega será sendos _pull request_ desde la rama del grupo a cada una de las ramas _master_ de cada alumno.
