const Either = () => {
  return {
    getType: () => "Either"
  };
};

const createRight = val => {
  return {
    ...Either(),
    val,
    map: function(fn) {
      this.val = fn(this.val);
      return this;
    },
    runEither(error, correct) {
      correct(this.val);
    },
    toString() {
      return `Right(${this.val})`;
    }
  };
};

const createLeft = val => {
  return {
    ...Either(),
    val,
    map(fn) {
      return this;
    },
    runEither(error, correct) {
      error(this.val);
    },
    toString() {
      return `Left(${this.val})`;
    }
  };
};

module.exports = { createLeft, createRight, Either };
