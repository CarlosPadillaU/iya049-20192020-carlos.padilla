const Either = function() {};

Either.prototype.getType = function() {
  return "Either";
};

const createRight = function(val) {
  this.val = val;
};

createRight.prototype = new Either();

createRight.prototype.map = function(fn) {
  this.val = fn(this.val);
  return this;
};

createRight.prototype.toString = function() {
  return `Right(${this.val})`;
};

createRight.prototype.runEither = function(error, correct) {
  correct(this.val);
};

const createLeft = function(val) {
  this.val = val;
};

createLeft.prototype = new Either();

createLeft.prototype.map = function(fn) {
  return this;
};

createLeft.prototype.toString = function() {
  return `Left(${this.val})`;
};

createLeft.prototype.runEither = function(error, correct) {
  error(this.val);
};

module.exports = { createLeft, createRight, Either };
