class Either {
  constructor() {}

  getType() {
    return "Either";
  }
}

class createRight extends Either {
  constructor(val) {
    super();
    this.val = val;
  }

  map(fn) {
    this.val = fn(this.val);

    return this;
  }

  runEither(error, correct) {
    correct(this.val);
  }

  toString() {
    return `Right(${this.val})`;
  }
}

class createLeft extends Either {
  constructor(val) {
    super();
    this.val = val;
  }

  map(fn) {
    return this;
  }

  runEither(error, correct) {
    error(this.val);
  }

  toString() {
    return `Left(${this.val})`;
  }
}

module.exports = { createLeft, createRight, Either };
