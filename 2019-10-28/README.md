# Ejercicio 10-28

El ejercicio consiste en implementar la función `reduce`.

## Fecha de entrega

El _pull request_ para la entrega del ejercicio debe crearse antes del comienzo de la clase del jueves día 31 de octubre.

## Enunciado

Modifica únicamente el cuerpo de la función `reduce` del archivo `index.js` para que los tests tengan éxito.

## Tecnologías

El ejercicio se debe realizar sin utilizar librerías externas.

## Entregables

Todos los archivos de esta carpeta son entregrables. La carpeta debe incluir, al menos,

El método de entrega será, para cada alumno, un _pull request_ a su rama _master_ (`/alumno/xxx.yyy/master`). El PR puede hacerse desde un _fork_ si es preciso.
