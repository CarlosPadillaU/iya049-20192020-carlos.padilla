//@ts-check
const http = require("http");
const url = require("url");
const fs = require("fs");

const PORT = process.env.PORT ? process.env.PORT : 3000;

let clients = [];

const getFilePath = path => {
  return __dirname + path;
};

//the routes of the server
const routes = {
  "/waitmove": (req, res) => {
    const headers = {
      "Content-Type": "text/event-stream",
      Connection: "keep-alive",
      "Cache-Control": "no-cache",
      "Access-Control-Allow-Origin": "*"
    };
    res.writeHead(200, headers);

    const clientId = Date.now();
    const newClient = {
      clientId,
      res
    };
    clients = [...clients, newClient];

    req.on("close", () => {
      console.log(` Connection closed`);
      clients = clients.filter(c => c.clientId !== clientId);
    });
  },
  "/makemove": (req, res) => {
    //get the query in a JSON obj
    const query = JSON.parse(
      '{"' +
        decodeURI(url.parse(req.url).query)
          .replace(/"/g, '\\"')
          .replace(/&/g, '","')
          .replace(/=/g, '":"') +
        '"}'
    );

    clients.forEach(client => {
      client.res.write(`data:${query.move}\n\n`);
    });

    res.write("Move make success");
    res.end();
  },
  "/": (req, res) => {
    //return the index html file
    fs.readFile(getFilePath("/index.html"), (err, content) => {
      console.log(err);

      res.writeHead(200, { "Content-Type": "text/html" });
      res.end(content, "utf-8");
    });
  }
};

const serveFiles = (req, res) => {
  const originalUrl = url.parse(req.url);

  fs.readFile(getFilePath(originalUrl.pathname), (err, content) => {
    if (!err) {
      let contentType = "";
      if (originalUrl.pathname.includes(".js")) {
        contentType = "text/javascript";
      }

      res.writeHead(200, { "Content-Type": contentType });

      res.end(content, "utf-8");
    } else {
      console.log(err);
    }
  });
};

http
  .createServer((req, res) => {
    const originalUrl = url.parse(req.url);

    //test if is file server request
    const regex = /\/src\/.+/gm;
    if (regex.test(originalUrl.pathname)) {
      serveFiles(req, res);
    } else {
      const fn = routes[originalUrl.pathname];

      if (fn) {
        fn(req, res);
      } else {
        res.write("Page not fount");
        res.end();
      }
    }
  })
  .listen(PORT, () => console.log(`http://localhost:${PORT}`));
