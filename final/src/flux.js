function todos(action, state = []) {
  switch (action.type) {
    case "ADD_TODO":
      return state.concat([action.text]);
    default:
      return state;
  }
}

(global => {
  const createStore = (reducer, initialState) => {
    return {
      _subscriber: () => {},
      state: initialState,
      dispatch(action) {
        const newState = reducer(action, this.state);
        this.state = newState;
        this._subscriber(newState);
      },
      subscribe(fn) {
        this._subscriber = fn;
        fn(this.state)
      },
      getState() {
        return this.state;
      }
    };
  };
  global.createStore = createStore;
})(window);
