(global => {
  const factoryPices = () => {
    const Icon = ({ name, color, id }) => {
      const div = document.createElement("div");
      div.draggable = true;
      const icon = document.createElement("i");
      icon.className = `fas fa-${name} pice-${color}`;

      div.appendChild(icon);

      div.ondragstart = ev => {
        ev.dataTransfer.setData("id", JSON.stringify({ id, color }));
      };

      return div;
    };

    const builders = {
      King(data) {
        return {
          render: () => Icon({ name: "chess-king", ...data })
        };
      },
      Queen(data) {
        return {
          render: () => Icon({ name: "chess-queen", ...data })
        };
      },
      Rook(data) {
        return {
          render: () => Icon({ name: "chess-rook", ...data })
        };
      },
      Bishop(data) {
        return {
          render: () => Icon({ name: "chess-bishop", ...data })
        };
      },
      Knight(data) {
        return {
          render: () => Icon({ name: "chess-knight", ...data })
        };
      },
      Pawn(data) {
        return {
          render: () => Icon({ name: "chess-pawn", ...data })
        };
      }
    };

    return {
      createPice(props) {
        const createEl = builders[props.type];
        if (createEl) {
          return createEl(props).render();
        }
      }
    };
  };

  global.Pices = factoryPices;
})(window);
