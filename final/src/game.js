//@ts-check
const COLORS = {
  WHITE: "white",
  BLACK: "black"
};

const PICES = {
  KING: "King",
  QUEEN: "Queen",
  ROOK: "Rook",
  BISHOP: "Bishop",
  KNIGHT: "Knight",
  PAWN: "Pawn"
};

const ACTIONS = {
  MOVEPICE: "MOVEPICE",
  ADDPICE: "ADDPICE"
};

const SERVER_URL = "";

const GAME_SIZE = 8;

const isPiceTook = (pos, picesState) => {
  const index = picesState.findIndex(
    pice =>
      pice.x == pos.x &&
      pice.y == pos.y &&
      pice.id != pos.id &&
      pice.color != pos.color &&
      !pice.isTook
  );

  if (index > 0) {
    const newPiceState = [...picesState];
    newPiceState[index] = { ...newPiceState[index], isTook: true };
    return newPiceState;
  }

  return picesState;
};

function reducer(action, state) {
  switch (action.type) {
    case ACTIONS.MOVEPICE:
      const { x, y, id } = action.payload;
      let mState = { ...state };

      mState.pices = isPiceTook(action.payload, mState.pices);

      const index = mState.pices.findIndex(pice => pice.id == id);
      mState.pices[index] = { ...mState.pices[index], x, y };

      return mState;
    case ACTIONS.ADDPICE:
      const newState = [...state];
      return newState;

    default:
      return state;
  }
}

function createBoard() {
  const boardArr = [];
  const board = document.querySelector(".board");

  for (let x = 1; x <= GAME_SIZE; x++) {
    const reverse = x % 2 == 0;
    const row = [];

    for (let index = 1; index <= GAME_SIZE; index++) {
      const div = document.createElement("div");

      let color;

      if (reverse) {
        color = index % 2 == 0 ? "white" : "black";
      } else {
        color = index % 2 == 1 ? "white" : "black";
      }

      div.className = `board-square ${color}`;
      div.innerHTML = `${x} - ${index}`;

      row.push(div);

      board.appendChild(div);
    }

    boardArr.push(row);
  }

  // document.getElementById("container").appendChild(board);

  return boardArr;
}

function setPices() {
  const defaultPices = [
    {
      type: PICES.ROOK,
      y: 7
    },
    {
      type: PICES.KNIGHT,
      y: 6
    },
    {
      type: PICES.BISHOP,
      y: 5
    },
    {
      type: PICES.KING,
      y: 4
    },
    {
      type: PICES.QUEEN,
      y: 3
    },
    {
      type: PICES.BISHOP,
      y: 2
    },
    {
      type: PICES.KNIGHT,
      y: 1
    },
    {
      type: PICES.ROOK,
      y: 0
    }
  ];

  const _generatePiceObj = (obj, key) => ({
    ...obj,
    id: `${obj.type}${obj.color}${key}`,
    isTook: false
  });

  const pices = [
    ...defaultPices.map((p, key) =>
      _generatePiceObj(
        {
          ...p,
          x: 0,
          color: COLORS.WHITE
        },
        key
      )
    ),
    ...Array.from(new Array(GAME_SIZE)).map((pawn, y) =>
      _generatePiceObj(
        {
          type: PICES.PAWN,
          color: COLORS.WHITE,
          y,
          x: 1
        },
        y
      )
    ),
    ...Array.from(new Array(GAME_SIZE)).map((pawn, y) =>
      _generatePiceObj(
        {
          type: PICES.PAWN,
          color: COLORS.BLACK,
          y,
          x: 6
        },
        y
      )
    ),
    ...defaultPices.map((p, key) =>
      _generatePiceObj(
        {
          ...p,
          x: 7,
          color: COLORS.BLACK
        },
        key
      )
    )
  ];

  return pices;
}

const initState = {
  board: createBoard(),
  pices: setPices()
};

const store = window.createStore(reducer, initState);
const { createPice } = window.Pices();

const listenEvents = () => {
  const events = new EventSource(`${SERVER_URL}/waitmove`);

  events.onmessage = ev => {
    store.dispatch({
      type: ACTIONS.MOVEPICE,
      payload: JSON.parse(ev.data)
    });
  };
};

listenEvents();

const sendMoveToServer = move => {
  const parseMove = Object.keys(move).reduce(
    (acc, key) => `${acc}${key}=${move[key]}&`,
    "?"
  );
  return new Promise(resolve => {
    fetch(`${SERVER_URL}/makemove?move=${JSON.stringify(move)}`)
      .then(response => response.json())
      .then(myJson => resolve(myJson));
  });
};

const chooseGameSide = () => {
  const board = document.querySelector(".board");
  const tookpices = document.querySelector(".tookpices");
  document.getElementById("sideBlack").addEventListener("click", () => {
    board.classList.remove("rotate");
    tookpices.classList.remove("rotate");
  });

  document.getElementById("sideWhite").addEventListener("click", () => {
    if (!board.classList.contains("rotate")) {
      board.classList.add("rotate");
      tookpices.classList.add("rotate");
    }
  });
};

chooseGameSide();

store.subscribe(state => {
  //show took pices
  document.getElementById("tookpices-white").innerHTML = "";
  state.pices
    .filter(pice => pice.isTook && pice.color == COLORS.BLACK)
    .forEach(pice => {
      document.getElementById("tookpices-white").appendChild(createPice(pice));
    });

  document.getElementById("tookpices-black").innerHTML = "";
  state.pices
    .filter(pice => pice.isTook && pice.color == COLORS.WHITE)
    .forEach(pice => {
      document.getElementById("tookpices-black").appendChild(createPice(pice));
    });

  //show pices in board
  state.board.forEach((x, xKey) => {
    x.forEach((y, yKey) => {
      y.innerHTML = "";

      y.ondrop = el => {
        const data = el.dataTransfer.getData("id");

        const { id, color } = JSON.parse(data);

        const payload = {
          id,
          color,
          x: xKey,
          y: yKey
        };
        sendMoveToServer(payload).then(() => {
          store.dispatch({
            type: ACTIONS.MOVEPICE,
            payload
          });
        });
      };
      y.ondragover = ev => {
        ev.preventDefault();
      };

      const pice = state.pices.find(
        pice => pice.x == xKey && pice.y == yKey && !pice.isTook
      );

      if (pice) {
        y.appendChild(createPice(pice));
      }
    });
  });
});
